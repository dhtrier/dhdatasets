---
title: "About"
---

This is a site listing datasets relevant to the Digital Humanities that are useful and suitably-sized for teaching key methods and approaches in Digital Humanities, such as data curation, data modeling, data analyis, visualization, statistics and programming. 

This collection is motivated by the observation that the above-mentioned skills are often taught using datasets and use cases that are relevant primarily to domains such as biology, social sciences or economics, and include datasets covering plants or cells, social media text and images, customer behavior, or insurance data. It appears desireable, however, to teach these skills to learners in the Digital Humanities using datasets that are directly relevant to research in our own field, that is derived from domains such as literature, history, art or musicology and involving data representing literary texts, historical paintings or bibliographic data as well as metadata relevant to these datasets or analytical data derived from such datasets. Such datasets will be collected, described and made available here.  

This site is maintained by Christof Schöch at Trier University, Germany. 
